import Vue from 'vue'
import store from '@/store'

export default {

  // Post request to send a duel invitation to another user
  requestDuel(rootState, userAskingId, userAskedId, challengeId, statusId, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/duels/request`,
      {user_asking_id: userAskingId, user_asked_id: userAskedId, 
      challenge_id: challengeId, status_id: statusId}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Post request to answer to a duel inviation
  answerDuel(rootState, duelId, statusId, callbacks)   {
    Vue.axios.put(`${rootState.baseUrl}/duels/answer`,
      {id: duelId, status_id: statusId}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Participates to a duel d
  participateDuel(rootState, duelId, userAskingScore, userAskedScore, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/duels/participate`,
      {id: duelId, user_asking_score: userAskingScore, user_asked_score: userAskedScore}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Gets a specific duel
  getDuel(rootState, id, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/duels/one-opposition/${id}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          } 
      }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          }
      });
  },

  // Gets all the user's duels
  getDuelsRequests(rootState, userId, statusId, size, page, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/duels/requests/${userId}/${statusId}/${size}/${page}`,{headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          }
      }) 
  } 
}
