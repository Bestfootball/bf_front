import Vue from 'vue'
import store from '@/store'

export default {
  // Get all clubs available in the bdd
  addTeam(rootState, teamId, coachId, clubId, clubName, teamName, teamLeagueName, categoryId, seasonId, numberPlayers, isUpdating, coachEmail, callbacks)   {
      Vue.axios.post(rootState.baseUrl + "/teams/add", {
        team_id: teamId, coach_id: coachId, 
        club_id: clubId, club_name: clubName, name: teamName,
        league: teamLeagueName, player_category_id: categoryId,
        season_id: seasonId, number_players_allowed: numberPlayers,
        is_updating: isUpdating, coach_email: coachEmail
      }, {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  { 
              if (callbacks.success)  { 
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }         
      });
  },

  // Get the list of teams
  getTeams(rootState, teamIdentifier, userId, roles, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/teams/list/${teamIdentifier}/${userId}/${roles}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Adds a player in a specific team
  addTeamPlayer(rootState, userId, teamId, positionId, callbacks)   {
      Vue.axios.put(`${rootState.baseUrl}/teams/add-player`, {
        user_id: userId, team_id: teamId, position_id: positionId}, {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  { 
              if (callbacks.success)  { 
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }         
      });
  },

  // Deletes a team 
  deleteTeam(rootState, teamId, clubId, callbacks)   {
    Vue.axios.delete(`${rootState.baseUrl}/teams/remove/${teamId}/${clubId}`, 
      {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.err) {
        callbacks.error(err)
      } 
    })
  },

  // Removes a player from his team
  removeTeamPlayer(rootState, userId, teamId, callbacks)   {
      Vue.axios.delete(`${rootState.baseUrl}/teams/remove-player/${userId}/${teamId}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },   
}
