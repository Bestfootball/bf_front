import Vue from 'vue'
import store from '@/store'

export default { 

  // User signs up
  localSignUp(rootState, email, username, password, sponsorship, role, callbacks)   {
      Vue.axios.post(`${rootState.userUrl}/sign-up`,
        {email: email, username: username, password: password, 
          sponsorship: sponsorship, roles: role},
          {headers: rootState.postHeaders})
      .then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Activates the user's account
  activateUser(rootState, username, sponsorship, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/activate`, 
        {username: username, sponsorship: sponsorship}, 
        {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // The user logs in
  localLogin(rootState, identifier, password, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/login`, 
        {identifier: identifier, password: password}, 
        {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Updates user's profile
  updateProfile(rootState, userId, username, snapchatUsername,
    instagramUsername, facebookUsername, youtubeUsername, 
    tiktokUsername, birthdate, gender, club, categoryId,
    positionId, preferredFootId, phoneNumber, coachExperience, representativePhone, league, address, city,
    country, roles, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/update-profile`,
        {user_id: userId, username: username, snapchat_username: snapchatUsername,
        instagram_username: instagramUsername, facebook_username: facebookUsername,
        youtube_username: youtubeUsername, tiktok_username: tiktokUsername,
        birthdate: birthdate, gender: gender, club: club,
        player_category_id: categoryId, position_id: positionId,
        preferred_foot_id: preferredFootId, years_experience: coachExperience,
        phone_number: phoneNumber, 
        representative_phone: representativePhone, 
        league: league, address: address,
        city: city, country: country, roles: roles},
          {headers: rootState.postHeaders})
      .then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Updates the user's account
  updateAccount(rootState, userId, lastName, firstName, email, roles, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/update-account`,
        {user_id: userId, last_name: lastName,
        first_name: firstName, email: email, roles: roles},
        {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  }, 

  // Updates the user's password
  updatePassword(rootState, userId, password, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/update-password`,
        {user_id: userId, password: password},
        {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Get a specific user and all the information about him
  getUser(rootState, identifier, roles, callbacks)   {
      Vue.axios.get(`${rootState.userUrl}/profile-information/${identifier}/${roles}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Get all users 
  async getUsers(rootState, callbacks)   {
    try {
      const response = await Vue.axios.get(rootState.userUrl + "/get-all", {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  { 
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      }      
    }
  },

  // Get all users 
  async getRolesSpecificCharacteristics(rootState, roles, callbacks)   {
    try {
      const response = await Vue.axios.get(`${rootState.userUrl}/roles-characteristics/${roles}`, {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  { 
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      }      
    }
  },

  // Sends a mentor code to the user's friends
  sendMentorCode(rootState, username, email1, email2, email3, email4, email5, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/mentoring`,
        {mentor_username: username, email1: email1, email2: email2,
        email3: email3, email4: email4, email5: email5}, {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // The user asks for a new password
  askForPassword(rootState, email, callbacks)   {
      Vue.axios.post(`${rootState.userUrl}/password-forgotten`,
        {email: email}, {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // The user sets up a new password
  renewPassword(rootState, email, password, callbacks)   {
      Vue.axios.put(`${rootState.userUrl}/renew-password`,
        {email: email, password: password},
        {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Deletes the user's account
  deleteAccount(rootState, id, callbacks)   {
    Vue.axios.delete(`${rootState.userUrl}/account/${id}`, 
      {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.err) {
        callbacks.error(err)
      } 
    })
  }  
}
