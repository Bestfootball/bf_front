import Vue from 'vue'
import store from '@/store'

export default {

  // Get users who follow the user or who are followed by him
  getFollowersFollowing(rootState, username, type, size, page, callbacks)   {
    Vue.axios.get(`${rootState.baseUrl}/follow/list-of/${type}/${username}/${size}/${page}`, 
      {headers: rootState.headers})
    .then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      } 
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          }
    });
  },

  // Follows another user
  follow(rootState, userVisitedId, userId, callbacks)   {
      Vue.axios.put(`${rootState.baseUrl}/follow`, 
        {user_follower_id: userId, user_following_id: userVisitedId}, 
        {headers: rootState.postHeaders})
      .then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },

  // Checks if the user already follows another user
  checkFollowStatus(rootState, userVisitedId, userId, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/follow/check/${userVisitedId}/${userId}`, 
        {headers: rootState.headers})
      .then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  { 
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
          callbacks.error(err)
        }        
      });
  },   
}
