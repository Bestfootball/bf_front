import Vue from "vue";
import store from "@/store";

export default {
  // Gets all clubs available in the bdd
  getClubs(rootState, callbacks) {
    Vue.axios
      .get(rootState.baseUrl + "/clubs", { headers: rootState.headers })
      .then((response) => {
        if (response.data) {
          if (callbacks.success) {
            callbacks.success(response.data);
          }
        } else {
          if (callbacks.fail) {
            callbacks.fail();
          }
        }
      })
      .catch((err) => {
        if (callbacks.error) {
          callbacks.error(err);
        }
      });
  },

  // Gets the price to register one players
  getPlayerPrices(rootState, callbacks) {
    Vue.axios
      .get(rootState.baseUrl + "/clubs/player-prices", {
        headers: rootState.headers,
      })
      .then((response) => {
        if (response.data) {
          if (callbacks.success) {
            callbacks.success(response.data);
          }
        } else {
          if (callbacks.fail) {
            callbacks.fail();
          }
        }
      })
      .catch((err) => {
        if (callbacks.error) {
          callbacks.error(err);
        }
      });
  },

  // Updates the club's subscription plan
  updateSubscriptionType(rootState, clubId, planId, callbacks) {
    Vue.axios
      .put(
        `${rootState.baseUrl}/clubs/subscriptions/update-type`,
        {
          club_id: clubId,
          plan_id: planId,
        },
        { headers: rootState.postHeaders }
      )
      .then((response) => {
        if (response.data) {
          if (callbacks.success) {
            callbacks.success(response.data);
          }
        } else {
          if (callbacks.fail) {
            callbacks.fail();
          }
        }
      })
      .catch((err) => {
        if (callbacks.error) {
          callbacks.error(err);
        }
      });
  },
};
