import Vue from 'vue'
import store from '@/store'

export default {
    // Get the ranking of all users based on several filters
  getRanking(rootState, userId, teamsIds, seasonId, ageCategoryId, gender, countryId, section, categoryId, challengeId, size, page, callbacks)   {
    Vue.axios.get(`${rootState.baseUrl}/ranking/filter/${userId}/${teamsIds}/${seasonId}/${ageCategoryId}/${gender}/${countryId}/${section}/${categoryId}/${challengeId}/${size}/${page}`, {headers: rootState.headers}).then((response) => {
        if (response.data)  {
            if (callbacks.success)  {
                callbacks.success(response.data);
            }
        }
        else  {
            if (callbacks.fail)  {
                callbacks.fail();
            }
        }
    }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          } else {
            console.log(err)
          }
    });
  }
}
