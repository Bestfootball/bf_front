import Vue from 'vue'
import store from '@/store'

export default {

  // Post request to comment a video
  contactUs(rootState, userId, name, email, subject, message, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/contacts/form-page`,
      {userId: userId, name: name, email: email, subject: subject, message: message}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  }
}
