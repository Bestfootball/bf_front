import Vue from 'vue'
import store from '@/store'

export default {

  // Get a specific gift 
  buyGift(rootState, userId, giftId, names, street, additionalAddressInfos, zipcode, city, country, email, phoneNumber, giftPrice, roles, callbacks)   { 
      Vue.axios.post(`${rootState.baseUrl}/gifts/buy-one`,
      {user_id: userId, gift_id: giftId, names: names,
      street: street, additional_address_infos: additionalAddressInfos,
      zipcode: zipcode, city: city, country: country,
      phone_number: phoneNumber, gift_price: giftPrice, roles: roles},
      {headers: rootState.postHeaders}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          } else {
            console.log(err)
          }
      });
  },

  // Get a specific gift 
  getGift(rootState, name, callbacks)   { 
      Vue.axios.get(`${rootState.baseUrl}/gifts/presentation/${name}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          } else {
            console.log(err)
          }
      });
  },

  // Gets all the gifts 
  getGifts(rootState, forClubs, userId, size, pageNumber, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/gifts/list/${forClubs}/${userId}/${size}/${pageNumber}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
          if(callbacks.error) {
            callbacks.error(err)
          } else {
            console.log(err)
          }
      });
  },  
}
