import Vue from 'vue'
import store from '@/store'

export default {

  // Participates to a training challenge
  participateChallenge(rootState, videoId, userId, videoName, challengesIds, categoryId, score, callbacks)   {
    Vue.axios.put(`${rootState.baseUrl}/trainings/videos/participate`,
      {id: videoId, user_id: userId, name: videoName, 
      challenges_ids: challengesIds, challenge_category_id: categoryId,
      score: score}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // POST request to create a new challenge
  createChallenge(rootState, challengeId, userId, name, description, categoryId, tutorialPartner, tutorialPartnerLink, tutorialVideoLink, teamsIds, levels, rewardChoice, challengeCoachReward, bfPath, challengeIdToUse, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/trainings/challenges/user-proposition`,
      {id: challengeId, user_id: userId, name: name,
      french_description: description, english_description: description, challenge_category_id: categoryId,
      tutorial_partner: tutorialPartner,
      tutorial_partner_link: tutorialPartnerLink,
      tutorial_video_link: tutorialVideoLink, teams_ids:teamsIds, levels: levels,
      reward_choice: rewardChoice, challenge_coach_reward: challengeCoachReward,
      bf_path: bfPath, challenge_id_to_use: challengeIdToUse}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // POST request to create a new challenge tutorial
  createChallengeTutorial(rootState, tutorialId, name, categoryId, userId, description, tutorialPartner, tutorialPartnerLink, teamsIds, bfPath, challengeId, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/trainings/tutorials/user-proposition`,
      {id: tutorialId, name: name, challenge_category_id: categoryId,
      user_id: userId, french_description: description, english_description: description, 
      tutorial_partner: tutorialPartner,
      tutorial_partner_link: tutorialPartnerLink, teams_ids:teamsIds, 
      bf_path: bfPath, challenge_id: challengeId}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // POST request to create a new coach exercice
  createCoachExercice(rootState, name, moduleId, moduleName, description, dateForExercice, teamsIds, callbacks)   {
    Vue.axios.post(`${rootState.baseUrl}/trainings/coach-exercices/user-proposition`,
      {name: name, coach_exercice_module_id: moduleId, module_name: moduleName,
      french_description: description, english_description: description, date: dateForExercice, teams_ids:teamsIds}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // PUT request to update the player score on a specific coach exercice
  updateCoachExercicePlayerScore(rootState, exerciceId, userId, success, callbacks)   {
    Vue.axios.put(`${rootState.baseUrl}/trainings/coach-exercices/player-score/update`,
      {coach_exercice_id: exerciceId, user_id: userId, success: success}, 
      {headers: rootState.postHeaders}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Get the list of all challenges depending on the chosen category 
  getChallenges(rootState, name, teamsIds, seasonId, ageCategoryId, categoryId, size, page, order, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/trainings/challenges/filter-by/${name}/${teamsIds}/${seasonId}/${ageCategoryId}/${categoryId}/${size}/${page}/${order}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }           
      });
  },

  // Get the list of all challenges depending on the chosen category 
  getChallengesTutorials(rootState, name, teamsIds, seasonId, ageCategoryId, categoryId, challengeId, size, page, order, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/trainings/tutorials/filter-by/${name}/${teamsIds}/${seasonId}/${ageCategoryId}/${categoryId}/${challengeId}/${size}/${page}/${order}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }           
      });
  },

  // Get all videos depending on the challenge and the category or just one specific video
  getChallengesVideos(rootState, userId, teamsIds, seasonId, ageCategoryId, categoryId, challengeId, videoId, watched, size, page, callbacks)   {
    Vue.axios.get(`${rootState.baseUrl}/trainings/videos/filter-by/${userId}/${teamsIds}/${seasonId}/${ageCategoryId}/${categoryId}/${challengeId}/${videoId}/${watched}/${size}/${page}`, {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Get the list of all challenges depending on the chosen category 
  getCoachExercices(rootState, name, teamsIds, seasonId, ageCategoryId, moduleId, size, page, order, callbacks)   {
      Vue.axios.get(`${rootState.baseUrl}/trainings/coach-exercices/filter-by/${name}/${teamsIds}/${seasonId}/${ageCategoryId}/${moduleId}/${size}/${page}/${order}`, {headers: rootState.headers}).then((response) => {
          if (response.data)  {
              if (callbacks.success)  {
                  callbacks.success(response.data);
              }
          }
          else  {
              if (callbacks.fail)  {
                  callbacks.fail();
              }
          }
      }).catch((err) => {
        if(callbacks.error) {
          callbacks.error(err)
        }           
      });
  },

  // Gets the latest videos 
  getLatestChallengesVideos(rootState, size, callbacks)   {
    Vue.axios.get(`${rootState.baseUrl}/trainings/videos/latest-videos/${size}`, 
      {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Gets people who liked the video
  getWhoLikedVideo(rootState, videoId, callbacks)   {
    Vue.axios.get(`${rootState.baseUrl}/trainings/videos/users-who-liked/${videoId}`, 
      {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    })
  },

  // Get the category associated to a specific challenge
  async getChallengeCategories(rootState, challengeId, callbacks) {
    try {
      const response = await Vue.axios.get(`${rootState.baseUrl}/trainings/challenges/categories/${challengeId}`, {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    }
  },

  // Get the module associated to a specific coach exercice
  async getCoachExerciceModules(rootState, exerciceId, callbacks) {
    try {
      const response = await Vue.axios.get(`${rootState.baseUrl}/trainings/coach-exercices/modules/${exerciceId}`, {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    }
  }, 

  // Get the coach's challenge statistics
  async getChallengeStats(rootState, challengeId, userId, size, callbacks) {
    try {
      const response = await Vue.axios.get(`${rootState.baseUrl}/trainings/challenges/stats/${challengeId}/${userId}/${size}`, {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    }
  },

  // Get the coach's exercice statistics
  async getCoachExerciceStats(rootState, userId, exerciceId, callbacks) {
    try {
      const response = await Vue.axios.get(`${rootState.baseUrl}/trainings/coach-exercices/stats/${userId}/${exerciceId}`, {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
      if(callbacks.error) {
        callbacks.error(err)
      } 
    }
  },

  // Deletes a challenge video
  deleteChallengeVideo(rootState, videoId, type, callbacks)   {
    Vue.axios.delete(`${rootState.baseUrl}/trainings/${type}/remove/${videoId}`, 
      {headers: rootState.headers}).then((response) => {
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail(response);
          }
      }
    }).catch((err) => {
      if(callbacks.err) {
        callbacks.error(err)
      } 
    })
  }    
}
