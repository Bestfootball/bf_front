import Vue from 'vue'
import store from '@/store'

export default {
  // Get all countries available in the bdd
  async getCountries(rootState, callbacks)   {
    try {
      const response = await Vue.axios.get(rootState.baseUrl + "/search/countries/list", {headers: rootState.headers})
      if (response.data)  {
          if (callbacks.success)  {
              callbacks.success(response.data);
          }
      }
      else  {
          if (callbacks.fail)  {
              callbacks.fail();
          }
      }
    } catch(err) {
        if(callbacks.error) {
          callbacks.error(err)
        }
    }
  }
}